import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StripepaymentsuccessComponent } from './stripepaymentsuccess.component';

describe('StripepaymentsuccessComponent', () => {
  let component: StripepaymentsuccessComponent;
  let fixture: ComponentFixture<StripepaymentsuccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StripepaymentsuccessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StripepaymentsuccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
