
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import {Router} from '@angular/router';
import { AuthService } from '../../service/auth.service';
@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private auth: AuthService,
    private myRoute: Router){
  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      let role = localStorage.getItem("role");
    if(this.auth.isLoggednIn() && role == 'user'){
      return true;
    }else if(this.auth.isLoggednIn() && role == 'admin'){
      this.myRoute.navigate(["/dashboard/dashboard"]);
      return false;
    }else{
        this.myRoute.navigate(["/login"]);
        return false;
      }
  }
}

@Injectable()
export class AdminAuthGuard implements CanActivate {
  constructor(private auth: AuthService,
    private myRoute: Router){
  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      let role = localStorage.getItem("role");
    if(this.auth.isLoggednIn() && role == 'admin'){
      return true;
    }else if(this.auth.isLoggednIn() && role == 'user'){
      this.myRoute.navigate(["/"]);
      return false;
    }else{
        this.myRoute.navigate(["/login"]);
        return false;
      }
  }
}
