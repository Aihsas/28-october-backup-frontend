import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../router.animations';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { SignupService } from '../service/signup.service';

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.scss'],
    animations: [routerTransition()]
})
export class SignupComponent implements OnInit {
    registerForm: FormGroup;
    submitted = false;
    listArr: any = [];
    constructor( private signupService:SignupService,private formBuilder: FormBuilder) { 
        this.registerForm = this.formBuilder.group({
            name: ['', Validators.required],
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required, Validators.minLength(6)]]
        });
    }

    ngOnInit() {
        
    }
    get f() { return this.registerForm.controls; }

    // convenience getter for easy access to form fields
    //get f() { return this.registerForm.controls; }

    onSubmit() {
        console.log("Registration form :", this.registerForm.value)
        this.submitted = true;
        if (this.registerForm.invalid) {
            return;
        }

        alert('Registered Succesfully')

        // stop here if form is invalid

        this.signupService
            .onSubmit(this.registerForm.value)
            .subscribe(data => {

            })
        // console.log(this.registerForm.value)
    }



}


