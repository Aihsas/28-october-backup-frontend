import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AuthGuard, AdminAuthGuard } from './shared';
import { HomeComponent } from './home/home.component';
import { UserdashboardComponent } from './userdashboard/userdashboard.component';
import { ViewblogComponent } from './viewblog/viewblog.component';
import { ViewproductComponent } from './viewproduct/viewproduct.component';
import { SignuppComponent } from './signupp/signupp.component';
import { ViewmoreComponent } from './viewmore/viewmore.component';
import { AddcartComponent } from './addcart/addcart.component';
import { ShippingaddressComponent } from './shippingaddress/shippingaddress.component';
import { StripecheckoutComponent } from './stripecheckout/stripecheckout.component';
import { StripepaymentsuccessComponent } from './stripepaymentsuccess/stripepaymentsuccess.component';
//import { AddblogsComponent } from './addblogs/addblogs.component';
//import { DashboardComponent } from './layout/dashboard/dashboard.component';
//
const routes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'login', loadChildren: './login/login.module#LoginModule' },
    // { path: 'signup', loadChildren: './signup/signup.module#SignupModule' },
    { path: 'layout', loadChildren: './layout/layout.module#LayoutModule', canActivate: [AdminAuthGuard] },
    { path: 'dashboard', loadChildren: './layout/layout.module#LayoutModule', canActivate: [AdminAuthGuard] },
    { path: 'error', loadChildren: './server-error/server-error.module#ServerErrorModule' },
    { path: 'access-denied', loadChildren: './access-denied/access-denied.module#AccessDeniedModule' },
    { path: 'tables', loadChildren: './layout/tables/tables.module#TablesModule', canActivate: [AuthGuard] },
    { path: 'charts', loadChildren: './layout/charts/charts.module#ChartsModule', canActivate: [AuthGuard] },
    { path: 'forms', loadChildren: './layout/charts/charts.module#ChartsModule', canActivate: [AuthGuard] },
    {
        path: 'userdashboard', component: UserdashboardComponent, canActivate: [AuthGuard]
    },
    {
        path: 'viewblog', component: ViewblogComponent, canActivate: [AuthGuard]
    },
    {
        path: 'viewproduct', component: ViewproductComponent, canActivate: [AuthGuard]
    },
    {
        path: 'home', component: HomeComponent
    },
    {
        path: 'signupp', component: SignuppComponent,
    },
    {
        path: 'viewmore', component: ViewmoreComponent, canActivate: [AuthGuard]
    },
    {
        path: 'addcart', component: AddcartComponent, canActivate: [AuthGuard]
    },
    {
        path: 'shipping', component: ShippingaddressComponent, canActivate: [AuthGuard]
    }
    ,
    {
        path: 'stripecheckout', component: StripecheckoutComponent, canActivate: [AuthGuard]
    },
    {
        path: 'stripepaymentsuccess', component: StripepaymentsuccessComponent, canActivate: [AuthGuard]
    }








    ,{ path: 'not-found', loadChildren: './not-found/not-found.module#NotFoundModule' },
    { path: '**', redirectTo: '/not-found' },


];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
