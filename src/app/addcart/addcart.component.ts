import { Component, OnInit, Input } from '@angular/core';
import { FormControl,FormGroup,FormBuilder } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CartlistService } from '../service/cartlist.service';
import { DeletecartproductService } from '../service/deletecartproduct.service';

@Component({
  selector: 'app-addcart',
  templateUrl: './addcart.component.html',
  styleUrls: ['./addcart.component.scss']
})
export class AddcartComponent implements OnInit {
  total: number;
  totalprice: number;
  constructor(public formBuilder:FormBuilder,public httpClient: HttpClient,private cartlist:CartlistService,private deletecartproducts:DeletecartproductService) { }
  addCart: FormGroup;
  listArr: any = [];
  addcartlist;
  quantity:number;
  status=true;
  
  increment(quantity) {
    // debugger;
    console.log(quantity)
    quantity=quantity+1;
    console.log(quantity);
    
  }
  decrement(quantity) {
    console.log(quantity)
    quantity=quantity-1;
    console.log(quantity);

  }
  
  ngOnInit() {
    this.addCart = new FormGroup({
      "pname": new FormControl(''),
      "pcost": new FormControl(''),
      "pdescription": new FormControl(''),
      "file": new FormControl(''),
      // "quantity": new FormControl(''), 
  }),
  
  this.addcartlisitng()

  }
  
  addcartlisitng(){
    var id =localStorage.getItem('userId');
    console.log(id)
    this.cartlist.addCart(id).subscribe(res=>{
      console.log("res comment.........",res)
      if(res == 0){
        this.status=true;
      }else{
        this.status=false;
        
      }
      this.listArr = res;
     let payementamount =0;
     this.listArr.forEach((value,index) => {
      payementamount+=this.listArr[index].pcost
     });
     this.totalprice=0
     this.totalprice=payementamount
     localStorage.setItem("amount",(this.totalprice).toString())
     console.log('Total Price======',this.totalprice)
    })


    // this.total += list.product.price * list.quantity;

    
  }

  deletecartproduct(id){
    this.deletecartproducts.deletecartproduct(id).subscribe(res => {
      console.log('Deleted');    
    location.reload()
    });
  }
            
  }


