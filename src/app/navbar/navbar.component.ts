import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  isLoggedIn: boolean = false;
  username: String = localStorage.getItem('username');
  role: String = localStorage.getItem('role');
  constructor() {
    let authToken = localStorage.getItem("Angular5Demo-Token");
    if (authToken) {
      this.isLoggedIn = true;
    }
  }

  ngOnInit() {
  }
  logout() {
    localStorage.removeItem("Angular5Demo-Token")
    this.isLoggedIn = false
  }



}
