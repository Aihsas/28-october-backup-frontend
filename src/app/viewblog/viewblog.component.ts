import { Component, OnInit } from '@angular/core';
import { FormControl,FormGroup,FormBuilder } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AddblogService } from '../service/addblog.service';
import {BlogCommentService } from '../service/blogcomment.service';
import { CommentlistingService } from '../service/commentlisting.service';
import {MessageService} from 'primeng/api';

@Component({
  selector: 'app-viewblog',
  templateUrl: './viewblog.component.html',
  styleUrls: ['./viewblog.component.scss']
})
export class ViewblogComponent implements OnInit {
  val1: number;
  isLoggedIn : boolean = false;
  addcomment:FormGroup
  submitted = false;
  listArr: any = [];
  commentList;
  blogList = new Array;
  constructor(public formBuilder:FormBuilder, public httpClient: HttpClient,private blog:AddblogService,private comment:BlogCommentService,private bloglisitng:CommentlistingService,private messageService: MessageService) { 
    let authToken = localStorage.getItem("Angular5Demo-Token");
    // console.log("auth : ", authToken)
    if(authToken) {
      this.isLoggedIn = true;
    }
  }

  ngOnInit() {
  this.addcomment = new FormGroup({
    "comment": new FormControl(''),
})
  this.httpClient.get("http://localhost:3000/bloglisting").subscribe(data=> {
  console.log("response: ", data)
  this.listArr = data;
})
  
// this.bloglisitng.commentListing(this.addcomment.value)
//         .subscribe(data => {

//         })
  }
  openModal(id){
    localStorage.setItem('blogId', id);
    document.getElementById('id01').style.display='block';
    this.blog.getBlogById(id).subscribe(res=>{
      console.log("res",res)
      this.blogList= res;
      this.getCommentList(id)
    })
  }
  logout(){
    localStorage.removeItem("Angular5Demo-Token")
    this.isLoggedIn = false
  }

  addCom() {
    console.log("Comment :", this.addcomment.value)
    this.submitted = true;
    var blogId=localStorage.getItem("blogId");
    this.addcomment.value.blogId=blogId;
    var userId=localStorage.getItem("userId");
    this.addcomment.value.userId=userId;
    this.addcomment.value.rating=this.val1;
    console.log("hgfvcdhjfvbdhbhdb",this.addcomment.value)
    // stop here if form is invalid

    this.comment
        .addCom(this.addcomment.value)
        .subscribe(data => {

        })
        this.messageService.add({severity:'success', summary:'Comment', detail:'Added Succesfully'});
          // location.reload();
       
    // console.log(this.registerForm.value)
}


getCommentList(id){
  // var id =localStorage.getItem('blogId');
  console.log(id)
  this.bloglisitng.commentListing(id).subscribe(res=>{
    console.log("res comment.........",res)
    this.commentList = res;
  })
          
}
 
}