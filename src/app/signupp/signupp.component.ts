import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../router.animations';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { SignupService } from '../service/signup.service';
import {MessageService} from 'primeng/api';


@Component({
  selector: 'app-signupp',
  templateUrl: './signupp.component.html',
  styleUrls: ['./signupp.component.scss']
})
export class SignuppComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  listArr: any = [];
  constructor(private signupService:SignupService,private formBuilder: FormBuilder,private messageService: MessageService) {
    this.registerForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email, Validators.pattern(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i)]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    })
   }
   get f() { return this.registerForm.controls; }
  ngOnInit() {
  }

onSubmit() {
  console.log("Registration form :", this.registerForm.value)
  this.submitted = true;
  if (this.registerForm.invalid) {
      return;
  }


  // stop here if form is invalid

  this.signupService
      .onSubmit(this.registerForm.value)
      .subscribe(data => {
        if(data.code==200){
          this.messageService.add({severity:'success', summary:'User', detail:'Added Succesfully'});

        }
        else{
          this.messageService.add({severity:'error', summary: 'EMAIL', detail:'Already Exist'});

        }

      })
  // console.log(this.registerForm.value)
  

}
}









