import { TestBed, inject } from '@angular/core/testing';

import { DeleteuserService } from './deleteuser.service';

describe('DeleteuserService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DeleteuserService]
    });
  });

  it('should be created', inject([DeleteuserService], (service: DeleteuserService) => {
    expect(service).toBeTruthy();
  }));
});
