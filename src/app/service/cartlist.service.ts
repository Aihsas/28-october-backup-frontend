import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class CartlistService {
  constructor(private http:HttpClient) { }
  addCart(id):Observable<any>{
    console.log("Comment data : ",id)
    
    return this.http.get("http://localhost:3000/listAddToCart?id="+id);
    //localhost:27017/market
}
}