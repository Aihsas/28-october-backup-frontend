import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs'
@Injectable({
  providedIn: 'root'
})
export class StripeService {
  constructor(private http:HttpClient) { }
  payment(data):Observable<any>{
    console.log("in stripe: ", data)
    
    return this.http.post("http://localhost:3000/generateCardToken",data);
    //localhost:27017/market
}

}
  
