import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class AdditemService {
  constructor(private http:HttpClient) { }
  addProducts(data):Observable<any>{
    console.log("login form data : ", data)
    
    return this.http.post("http://localhost:3000/product",data);
}

}
