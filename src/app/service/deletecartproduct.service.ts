import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class DeletecartproductService {
  constructor(private http:HttpClient) { }
  deletecartproduct(id):Observable<any>{
    return this.http.delete("http://localhost:3000/deletecartproduct?id="+id);
  }
}
