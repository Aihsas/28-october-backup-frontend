import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class AddblogService {
  constructor(private http:HttpClient) { }
  addBlogs(data):Observable<any>{
    console.log("blog form data : ", data)
    
    return this.http.post("http://localhost:3000/addblog",data);
}

getBlogById(id):Observable<any>{
  return this.http.get("http://localhost:3000/getBlogBYid?id="+id);
}

}

  