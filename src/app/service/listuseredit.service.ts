import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ListusereditService {
 
  constructor(private http:HttpClient) { }
  //=======product services
  editUsers(id, data):Observable<any>{
      return this.http.put('http://localhost:3000/edituserlist?id='+id,data)
  }
  getroleBYid(id):Observable<any>{
    return this.http.get("http://localhost:3000/getRoleBYid?id="+id);
  }
  
  }
  