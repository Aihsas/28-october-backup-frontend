import { TestBed, inject } from '@angular/core/testing';

import { CommentlistingService } from './commentlisting.service';

describe('CommentlistingService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CommentlistingService]
    });
  });

  it('should be created', inject([CommentlistingService], (service: CommentlistingService) => {
    expect(service).toBeTruthy();
  }));
});
