import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class BlogCommentService {
  constructor(private http:HttpClient) { }
  addCom(data):Observable<any>{
    console.log("Comment data : ", data)
    
    return this.http.post("http://localhost:3000/addblogcomment",data);
    //localhost:27017/market
}

}