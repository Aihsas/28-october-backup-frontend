import { TestBed, inject } from '@angular/core/testing';

import { StripecheckoutService } from './stripecheckout.service';

describe('StripecheckoutService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StripecheckoutService]
    });
  });

  it('should be created', inject([StripecheckoutService], (service: StripecheckoutService) => {
    expect(service).toBeTruthy();
  }));
});
