import { TestBed, inject } from '@angular/core/testing';

import { CartlistService } from './cartlist.service';

describe('CartlistService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CartlistService]
    });
  });

  it('should be created', inject([CartlistService], (service: CartlistService) => {
    expect(service).toBeTruthy();
  }));
});
