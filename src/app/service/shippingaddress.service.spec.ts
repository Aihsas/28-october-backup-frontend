import { TestBed, inject } from '@angular/core/testing';

import { ShippingaddressService } from './shippingaddress.service';

describe('ShippingaddressService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ShippingaddressService]
    });
  });

  it('should be created', inject([ShippingaddressService], (service: ShippingaddressService) => {
    expect(service).toBeTruthy();
  }));
});
