import { TestBed, inject } from '@angular/core/testing';
import { BlogCommentService } from './blogcomment.service';


describe('BlogcommentService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BlogCommentService]
    });
  });

  it('should be created', inject([BlogCommentService], (service: BlogCommentService) => {
    expect(service).toBeTruthy();
  }));
});
