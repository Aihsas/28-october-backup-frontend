import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class EdititemService {

  constructor(private http:HttpClient) { }


//=======product services
  editProducts(id, data):Observable<any>{
    return this.http.put('http://localhost:3000/editlistitem?id='+id,data)
}
getprodBYid(id):Observable<any>{
  return this.http.get("http://localhost:3000/getProductBYid?id="+id);
}

}




      
