import { TestBed, inject } from '@angular/core/testing';

import { ListusereditService } from './listuseredit.service';

describe('ListusereditService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ListusereditService]
    });
  });

  it('should be created', inject([ListusereditService], (service: ListusereditService) => {
    expect(service).toBeTruthy();
  }));
});
