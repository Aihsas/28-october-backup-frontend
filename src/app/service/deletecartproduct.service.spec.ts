import { TestBed, inject } from '@angular/core/testing';

import { DeletecartproductService } from './deletecartproduct.service';

describe('DeletecartproductService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DeletecartproductService]
    });
  });

  it('should be created', inject([DeletecartproductService], (service: DeletecartproductService) => {
    expect(service).toBeTruthy();
  }));
});
