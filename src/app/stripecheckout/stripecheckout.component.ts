import { Component, AfterViewInit, OnDestroy, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { StripeService } from '../service/stripe.service';
import { MessageService } from 'primeng/api';
import { Router } from '@angular/router';
import { StripecheckoutService } from '../service/stripecheckout.service';

@Component({
  selector: 'app-stripecheckout',
  templateUrl: './stripecheckout.component.html',
  styleUrls: ['./stripecheckout.component.scss']
})
export class StripecheckoutComponent implements AfterViewInit, OnDestroy {

  @ViewChild('cardInfo') cardInfo: ElementRef;

  card: any;
  cardHandler = this.onChange.bind(this);
  error: string;
  amt: any
  userID: any = localStorage.getItem("userId")
  constructor(private cd: ChangeDetectorRef, private stripee: StripeService, private messageService: MessageService, private router: Router, private stripecheckout: StripecheckoutService
  ) { }
  ngOnInit() {
    this.amt = localStorage.getItem("amount")


  }
  ngAfterViewInit() {
    this.card = elements.create('card');
    this.card.mount(this.cardInfo.nativeElement);

    this.card.addEventListener('change', this.cardHandler);
  }

  ngOnDestroy() {
    this.card.removeEventListener('change', this.cardHandler);
    this.card.destroy();
  }

  onChange({ error }) {
    if (error) {
      this.error = error.message;
    } else {
      this.error = null;
    }
    this.cd.detectChanges();
  }

  async onSubmit(form: NgForm) {
    const { token, error } = await stripe.createToken(this.card);
    //var amt=localStorage.getItem("amount")
    var email = localStorage.getItem("LoggedInUser")

    if (error) {
      console.log('Something is wrong:', error);
    } else {
      console.log('Success!', token);
      var data = {
        token: token.id,
        amount: this.amt,
        LoggedInUser: email,

      }
      // send the token to the your backend to process the charge
      this.stripee.payment(data).subscribe(res => {
        if (error){throw error} 
        else{
          console.log("success==========================")
          // this.messageService.add({ severity: 'success', summary: 'Payment', detail: 'Done Succesfully' });
          this.router.navigate(['/stripepaymentsuccess'])
          

        }
      })
      this.stripecheckout.payment(this.userID).subscribe(res => {
        console.log('Deleted');
        // location.reload()
      });
    }
  }

}

