import { Component, OnInit } from '@angular/core';
import { FormControl,FormGroup,FormBuilder } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { AddcartService } from '../service/addcart.service';
import {MessageService} from 'primeng/api';

@Component({
  selector: 'app-viewproduct',
  templateUrl: './viewproduct.component.html',
  styleUrls: ['./viewproduct.component.scss']
})
export class ViewproductComponent implements OnInit {
  isLoggedIn : boolean = false;
  addtocart: any;

  constructor(public httpClient: HttpClient,private router: Router,private cart:AddcartService,private messageService: MessageService) { 
    let authToken = localStorage.getItem("Angular5Demo-Token");
    console.log("auth : ", authToken)
    if(authToken) {
      this.isLoggedIn = true;
    }
  }
  addproduct: FormGroup;
  addcart:FormGroup
  submitted = false;
  listArr: any = [];
  ngOnInit() {
    this.addproduct = new FormGroup({
      "pname": new FormControl(''),
      "pdescription": new FormControl(''),
      "pcost": new FormControl(''),
      "status": new FormControl(''),
      
      
  }),
  this.addcart = new FormGroup({
  })


  this.httpClient.get("http://localhost:3000/listing").subscribe(data=> {
  console.log("response: ", data)
  this.listArr = data;

})
}
logout(){
  localStorage.removeItem("Angular5Demo-Token")
  this.router.navigate(["home"]);
  this.isLoggedIn = false
}
addCart(list) {
    console.log("Cart :", this.addcart.value)
    this.submitted = true;
    var pcost;
    var pname;
    var productid;
    this.addcart.value.productid=list._id;
    var userId=localStorage.getItem("userId");
    this.addcart.value.userId=userId;
    this.addcart.value.pcost=list.pcost;
    this.addcart.value.pname=list.pname;
    this.addcart.value.file=list.file;

    // this.addcart.value.quantity=list.quantity;

    console.log("hgfvcdhjfvbdhbhdb",this.addcart.value)

    this.cart
        .addCart(this.addcart.value)
        .subscribe(data => {

        })
        
        this.messageService.add({severity:'success', summary:'Product', detail:'Added Succesfully'});
}

}

