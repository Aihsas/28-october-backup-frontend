import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { routerTransition } from '../router.animations';
import { FormGroup, Validators, FormControl ,FormBuilder} from '@angular/forms';
import { LoginService } from '../service/login.service';
import { AuthService } from '../service/auth.service';
import { MessageService } from 'primeng/api';


@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {
    submitted = false;
    listArr: any = [];
  loginForm: FormGroup;
    loginService: any;
  
    constructor( private loginservice:LoginService,private router: Router,private formBuilder: FormBuilder,private auth:AuthService,private messageService: MessageService) {
        this.loginForm= this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required, Validators.minLength(6)]]
        });
     }
    get f() { return this.loginForm.controls; }

    ngOnInit() {
       
    }

    onLogin() {
        if (this.loginForm.valid) {
            this.auth.sendToken(this.loginForm.value.email)
        console.log("Registration form :", this.loginForm.value)
        this.router.navigate(["home"]);
        this.submitted = true;
        this.loginservice
            .onLogin(this.loginForm.value)
            .subscribe(data => {
                console.log("data : ", data)
                if(data.code==200){
                localStorage.setItem("Angular5Demo-Token", data.token);
                localStorage.setItem("userId",data.data._id)
                localStorage.setItem("username",data.data.name);

                // this.router.navigate(['/dashboard']);
                //  return false;
                localStorage.setItem("role",data.data.role);
                if(localStorage.getItem("role")== "admin"){
                this.router.navigate(['/dashboard']);
 
                }
                
                else if(localStorage.getItem("role") == "user"){
                    this.router.navigate(['/viewproduct'])
                }
            }
else{
    this.messageService.add({ severity: 'error', summary: 'Password', detail: 'Wrong Password' });
}
            })
            // alert("wrong password")

    }
}

}

    
