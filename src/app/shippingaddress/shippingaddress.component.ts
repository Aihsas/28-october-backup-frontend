import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../router.animations';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { SignupService } from '../service/signup.service';
import { ShippingaddressService } from '../service/shippingaddress.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-shippingaddress',
  templateUrl: './shippingaddress.component.html',
  styleUrls: ['./shippingaddress.component.scss']
})
export class ShippingaddressComponent implements OnInit {
  shippingForm: FormGroup;
  submitted = false;
  listArr: any = [];
  constructor(private formBuilder: FormBuilder,private shippingaddress:ShippingaddressService,private router: Router,) {
    this.shippingForm = this.formBuilder.group({
      name: ['', Validators.required],
      address1: ['', Validators.required],
      address2: ['', Validators.required],
      contact: ['',  [Validators.required,Validators.minLength(10)]],
      pincode: ['', Validators.required],
      city: ['', Validators.required],
      // userId: ['', Validators.required],
  });
}
get f() { return this.shippingForm.controls; }

  ngOnInit() {
  }

  onship() {
    var userId =localStorage.getItem('userId');
    console.log(userId)
    this.shippingForm.value.userId=userId;

    console.log("Registration form :", this.shippingForm.value)
    this.submitted = true;
    if (this.shippingForm.invalid) {
        return;
    }
    this.shippingaddress
        .onship(this.shippingForm.value)
        .subscribe(data => {

        })
alert("Address Added")
    console.log(this.shippingForm.value)
    this.router.navigate(['/stripecheckout'])
}




}



