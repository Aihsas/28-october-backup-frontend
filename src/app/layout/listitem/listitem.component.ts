import { Component, OnInit } from '@angular/core';
import { FormControl,FormGroup,FormBuilder } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DeletelistitemService } from '../../service/deletelistitem.service';
import { identifierModuleUrl } from '@angular/compiler';
@Component({
  selector: 'app-listitem',
  templateUrl: './listitem.component.html',
  styleUrls: ['./listitem.component.scss']
})
export class ListitemComponent implements OnInit {

  constructor(public formBuilder:FormBuilder, public httpClient: HttpClient,private deletelistitemm:DeletelistitemService) { }
  addproduct: FormGroup;
  submitted = false;
  listArr: any;
  ngOnInit() {
    this.addproduct = new FormGroup({
    "pname": new FormControl(''),
    "pdescription": new FormControl(''),
    "pcost": new FormControl(''),
    "status": new FormControl(''),
    
  
})
this.httpClient.get("http://localhost:3000/listing").subscribe(data=> {
  console.log("response: ", data)
  this.listArr = data;
})
}
addProducts(){
  console.log(this.addproduct.value)
}
createAuthorizationHeader(headers: Headers) {
  headers.append('Content-Type', 'application/json; charset=utf-8');
  headers.append('Access-Control-Allow-Origin', '*');
}
login(){
  let headers = new Headers();
  this.createAuthorizationHeader(headers);
  let options: any = {
    headers: headers
  };
 
}
deletelist(id){
  this.deletelistitemm.deletelist(id).subscribe(res => {
    console.log('Deleted');    
  location.reload()
  });
}
}

  

