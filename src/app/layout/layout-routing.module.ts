import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';
import { ListitemComponent } from './listitem/listitem.component';
import { ListuserComponent } from './listuser/listuser.component';
import { ListitemeditComponent } from './listitemedit/listitemedit.component';
import { AddblogComponent } from './addblog/addblog.component';
import { ListblogComponent } from './listblog/listblog.component';
import { ViewbloggComponent } from './viewblogg/viewblogg.component';
import { AuthGuard } from '../shared';
import { ListusereditComponent } from './listuseredit/listuseredit.component';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'dashboard', pathMatch: 'prefix' },
            { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule',},
            { path: 'charts', loadChildren: './charts/charts.module#ChartsModule' },
            { path: 'tables', loadChildren: './tables/tables.module#TablesModule' },
            { path: 'forms', loadChildren: './form/form.module#FormModule'},
            { path: 'additem', loadChildren: './additem/additem.module#AdditemModule' },
            { path: 'bs-element', loadChildren: './bs-element/bs-element.module#BsElementModule' },
            { path: 'grid', loadChildren: './grid/grid.module#GridModule' },
            { path: 'components', loadChildren: './bs-component/bs-component.module#BsComponentModule' },
            { path: 'blank-page', loadChildren: './blank-page/blank-page.module#BlankPageModule' },
            { path: 'adduser', loadChildren: './adduser/adduser.module#AdduserModule' },
            {
                path:'listitem',component:ListitemComponent
            },
            {
                path:'listuser',component:ListuserComponent
            },
            {
                path:'addblog',component:AddblogComponent
            },
            {
                path:'listblog',component:ListblogComponent,
            },
            {
                path:'listitemedit/:id',component:ListitemeditComponent
            },
            {
                path:'viewblog',component:ViewbloggComponent,
            },
            {
                path:'listuseredit/:id',component:ListusereditComponent
            },
        

        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule {}
