import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { HeaderComponent } from './components/header/header.component';
//import { ListitemComponent } from './listitem/listitem.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ListitemComponent } from './listitem/listitem.component';
import { ListuserComponent } from './listuser/listuser.component';
import { ListitemeditComponent } from './listitemedit/listitemedit.component';
import { AddblogComponent } from './addblog/addblog.component';
import { ListblogComponent } from './listblog/listblog.component';
import { ViewbloggComponent } from './viewblogg/viewblogg.component';
import {PaginatorModule} from 'primeng/paginator';
import { AuthService } from '../service/auth.service';
import { AuthGuard, AdminAuthGuard } from '../shared';
import {TableModule} from 'primeng/table';
import {ButtonModule} from 'primeng/button';
import {ToastModule} from 'primeng/toast';
import { MessageService } from 'primeng/api';
import {ScrollPanelModule} from 'primeng/scrollpanel';
import { ListusereditComponent } from './listuseredit/listuseredit.component';
import {DropdownModule} from 'primeng/dropdown';
import {KeyFilterModule} from 'primeng/keyfilter';

@NgModule({
    imports: [FormsModule,ReactiveFormsModule,
        CommonModule,
        LayoutRoutingModule,
        TranslateModule,PaginatorModule,
        NgbDropdownModule.forRoot(),
        TableModule,
        ButtonModule,
        ToastModule,
        ScrollPanelModule,
        DropdownModule,
        KeyFilterModule
        
        
    ],
    declarations: [LayoutComponent, SidebarComponent, HeaderComponent, ListitemComponent, ListuserComponent, ListitemeditComponent, AddblogComponent, ListblogComponent,  ViewbloggComponent,ListusereditComponent]
    ,providers: [AuthGuard,AuthService,MessageService,AdminAuthGuard],
})
export class LayoutModule {}
