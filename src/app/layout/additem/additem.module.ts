import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdditemRoutingModule } from './additem-routing.module';
import { AdditemComponent } from './additem.component';
import { ReactiveFormsModule } from '@angular/forms'
import { FormsModule, FormGroup }   from '@angular/forms'
import {ToastModule} from 'primeng/toast';

@NgModule({
  imports: [
    CommonModule,AdditemRoutingModule,ReactiveFormsModule,FormsModule,ToastModule
 
  ],
  declarations: [AdditemComponent]
})
export class AdditemModule { }
