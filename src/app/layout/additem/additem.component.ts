import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl,FormBuilder } from '@angular/forms';
import { AdditemService } from '../../service/additem.service';
import {MessageService} from 'primeng/api';

@Component({
  selector: 'app-additem',
  templateUrl: './additem.component.html',
  styleUrls: ['./additem.component.scss']
})
export class AdditemComponent implements OnInit {
  addproduct: FormGroup;
  submitted = false;
  listArr: any = [];
  constructor(private additemService:AdditemService,private messageService: MessageService,private formBuilder: FormBuilder,private fb:FormBuilder) { }

  ngOnInit() {
    // this.addproduct = this.formBuilder.group({
    //   pname: ['', [Validators.required, Validators.minLength(4)]],
    //   pdescription: ['', [Validators.required, Validators.minLength(4)]],
    //   pcost: ['', Validators.required],
    //   status: ['', [Validators.required, Validators.minLength(3)]],
    this.addproduct = this.fb.group({
      pname: ['', Validators.required],
      pdescription: ['', Validators.required],
      pcost: ['', Validators.required],
      status: ['', Validators.required],
      avatar: ['',Validators.required]

  });
}
get f() { return this.addproduct.controls; }

addProducts() {
  const formModel = this.prepareSave();
  console.log("Registration form :", this.addproduct.value)
  this.submitted = true;
  if (this.addproduct.invalid) {
    return;
}
  // stop here if form is invalid

  this.additemService
      .addProducts(formModel)
      .subscribe(data => {

      })
  // console.log(this.registerForm.value)
  this.messageService.add({severity:'success', summary:'Product', detail:'Added Succesfully'});

}
onFileChange(event) {
  if(event.target.files.length > 0) {
    let file = event.target.files[0];
    this.addproduct.get('avatar').setValue(file);
  }
}
private prepareSave(): any {
  let input = new FormData();
  // This can be done a lot prettier; for example automatically assigning values by looping through `this.form.controls`, but we'll keep it as simple as possible here
  input.append('pname', this.addproduct.get('pname').value);
  input.append('pdescription', this.addproduct.get('pdescription').value);
  input.append('pcost', this.addproduct.get('pcost').value);
  input.append('status', this.addproduct.get('status').value);
  input.append('file', this.addproduct.get('avatar').value);

  return input;
}


}






  

