import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl,FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ListusereditService } from '../../service/listuseredit.service';
import {MessageService} from 'primeng/api';

@Component({
  selector: 'app-listuseredit',
  templateUrl: './listuseredit.component.html',
  styleUrls: ['./listuseredit.component.scss']
})
export class ListusereditComponent implements OnInit {
  edituserr: FormGroup;
  listuseredit: any;
  listArr: any = [];
  submitted: boolean;
  constructor(private router: Router,private listuserservice:ListusereditService,private fb:FormBuilder,private route:ActivatedRoute,private messageService: MessageService) { 
      this.edituserr= fb.group({
      "role": ['', Validators.required]

      
  });
  }
  get f() { return this.edituserr.controls; }

  ngOnInit() {
    const queryParams = this.route.snapshot.params['id']
    console.log("ffffffff",queryParams)
        this.listuserservice.getroleBYid(queryParams).subscribe((res:any)=>{
          console.log("Edit item::",res)
          if(res.code=200){
            //alert(res)
            this.edituserr.patchValue({role: res.role}); 
    
          }
        })
   
  }

  editUsers(){
    this.submitted = true;
  if (this.edituserr.invalid) {
    return;
}
    const formModel=this.prepareSave();
    this.route.params.subscribe(params =>{
      console.log("params.id=======",params.id)

      var obj = {
        id : params.id,
        data: this.edituserr.value
      }
      this.listuserservice.editUsers(params.id,formModel).subscribe((res:any)=>{
        console.log("Edit item::",res)
        if(res.code=200){
          this.messageService.add({severity:'success', summary:'User', detail:'Edited Succesfully'});

          // alert("Edited Successfully")
        }
      })
    })
    
  }
  private prepareSave(): any {
    let input = new FormData();
    input.append('role', this.edituserr.get('role').value);
    return input;
  }


}
