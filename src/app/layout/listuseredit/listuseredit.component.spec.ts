import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListusereditComponent } from './listuseredit.component';

describe('ListusereditComponent', () => {
  let component: ListusereditComponent;
  let fixture: ComponentFixture<ListusereditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListusereditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListusereditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
