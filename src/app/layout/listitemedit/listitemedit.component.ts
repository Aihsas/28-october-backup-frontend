import { Component, OnInit,ElementRef,ViewChild, Input} from '@angular/core';
import { FormGroup, Validators, FormControl,FormBuilder } from '@angular/forms';
import { AdditemService } from '../../service/additem.service';
import { EdititemService } from '../../service/edititem.service';
import { Router, ActivatedRoute } from '@angular/router';
import {MessageService} from 'primeng/api';

@Component({
  selector: 'app-listitemedit',
  templateUrl: './listitemedit.component.html',
  styleUrls: ['./listitemedit.component.scss']
})
export class ListitemeditComponent implements OnInit {
  editproduct: FormGroup;
  submitted = false;
  listArr: any = [];
  file:null;
  @ViewChild('fileInput') fileInput:ElementRef;
  constructor(private router: Router,private edititemService:EdititemService,
    private route:ActivatedRoute,private fb:FormBuilder,private messageService: MessageService) { 
      this.editproduct= fb.group({
        "pname":[null,Validators.required],
        "pdescription":[null,Validators.required],
        "pcost":[null,Validators.required],
        "status":[null,Validators.required],
        file:null

      });
    }

  ngOnInit() {
    const queryParams = this.route.snapshot.params['id']
console.log("ffffffff",queryParams)
    this.edititemService.getprodBYid(queryParams).subscribe((res:any)=>{
      console.log("Edit item::",res)
      if(res.code=200){
        //alert(res)
        this.editproduct.patchValue({pname: res.pname,pcost:res.pcost,pdescription:res.pdescription,status:res.status,file: res.file}); 

      }
    })

}
editProducts(){
  const formModel=this.prepareSave();
  this.route.params.subscribe(params =>{
    console.log(params.id)
    var obj = {
      id : params.id,
      data: this.editproduct.value
    }
    this.edititemService.editProducts(params.id,formModel).subscribe((res:any)=>{
      console.log("Edit item::",res)
      if(res.code=200){
        // alert("edited successfully")
        this.messageService.add({severity:'success', summary:'Item', detail:'Edited Succesfully'});

      }
    })
  })
  // console.log("data")
 
}

onFileChange(event) {
  if(event.target.files.length > 0) {
    let file = event.target.files[0];
    this.editproduct.get('file').setValue(file);
  }
}
private prepareSave(): any {
  let input = new FormData();
  // This can be done a lot prettier; for example automatically assigning values by looping through `this.form.controls`, but we'll keep it as simple as possible here
  input.append('pname', this.editproduct.get('pname').value);
  input.append('pdescription', this.editproduct.get('pdescription').value);
  input.append('pcost', this.editproduct.get('pcost').value);
  input.append('status', this.editproduct.get('status').value);
  input.append('file', this.editproduct.get('file').value);

  return input;
}



}
  
