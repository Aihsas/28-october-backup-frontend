import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators} from '@angular/forms';
import { AdduserService } from '../../service/adduser.service';
import { SignupService } from '../../service/signup.service';
import {MessageService} from 'primeng/api';
@Component({
  selector: 'app-adduser',
  templateUrl: './adduser.component.html',
  styleUrls: ['./adduser.component.scss']
})
export class AdduserComponent implements OnInit {
  submitted = false;
  listArr: any = [];
  adduser: FormGroup;
  constructor(private signupservices:SignupService,private formBuilder: FormBuilder,private messageService: MessageService) {}
    
  ngOnInit() {
    this.adduser = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email, Validators.pattern(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i)]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      role: ['', Validators.required],
      
  });
  

}
get f() { return this.adduser.controls; }

onSubmit(){
  console.log("Registration form :", this.adduser.value)
  this.submitted = true;
  if (this.adduser.invalid) {
    return;
}
  // stop here if form is invalid

  this.signupservices
      .onSubmit(this.adduser.value)
      .subscribe(data => {
        if(data.code==200){
          this.messageService.add({severity:'success', summary:'User', detail:'Added Succesfully'});

        }
        else{
          this.messageService.add({severity:'error', summary: 'Error Message', detail:'Already Exist'});

        }
      })
  // console.log(this.registerForm.value)
  // alert('User Added Successfully')
  


}
}









