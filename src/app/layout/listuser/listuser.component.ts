import { Component, OnInit } from '@angular/core';
import { FormControl,FormGroup,FormBuilder } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DeleteuserService } from '../../service/deleteuser.service';

@Component({
  selector: 'app-listuser',
  templateUrl: './listuser.component.html',
  styleUrls: ['./listuser.component.scss']
})
export class ListuserComponent implements OnInit {

  constructor(public formBuilder:FormBuilder, public httpClient: HttpClient,private deleteuse:DeleteuserService) {}
  adduser: FormGroup;
  submitted = false;
  listArr: any = [];
  ngOnInit() {
    this.adduser = new FormGroup({
      "name": new FormControl(''),
      "email": new FormControl(''),
      "password": new FormControl(''),
      "role": new FormControl(''),
  })
  this.httpClient.get("http://localhost:3000/createuserlist").subscribe(data=> {
  console.log("response: ", data)
  this.listArr = data;
})
}
addUsers(){
  console.log(this.adduser.value)
}
createAuthorizationHeader(headers: Headers) {
  headers.append('Content-Type', 'application/json; charset=utf-8');
  headers.append('Access-Control-Allow-Origin', '*');
}
login(){
  let headers = new Headers();
  this.createAuthorizationHeader(headers);
  let options: any = {
    headers: headers
  };
 
}
deleteuser(id){
  this.deleteuse.deleteuser(id).subscribe(res => {
    console.log('Deleted');    
  location.reload()
  });
}
}



